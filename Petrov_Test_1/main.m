//
//  main.m
//  Petrov_Test_1
//
//  Created by Anton Petrov on 11/17/16.
//  Copyright © 2016 Anton Petrov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (PetrovTestCategory)

- (void)swapObjectAtIndex:(NSUInteger)firstIndex withObjectAtIndex:(NSUInteger)secondIndex;

- (void)shellSortWithKey:(NSString *)key;

@end

@implementation NSMutableArray (PetrovTestCategory)

- (void)swapObjectAtIndex:(NSUInteger)firstIndex withObjectAtIndex:(NSUInteger)secondIndex {    
    if(firstIndex > self.count - 1 || secondIndex > self.count - 1) {
        NSLog(@"Attempt to enter incorrect index");
        exit(1);
    }
    NSMutableArray* tmp = self[firstIndex];
    self[firstIndex] = self[secondIndex];
    self[secondIndex] = tmp;
}

- (void)shellSortWithKey:(NSString *)key {
    NSUInteger d = self.count/2;
    while(d > 0)
    {
        for (NSUInteger i = d; i < self.count; i++)
        {
            NSMutableArray* temp = self[i];
            NSUInteger j;
            
            for (j = i; j >= d && [[self objectAtIndex:j] valueForKey:key]  > [temp valueForKey:key]; j -= d)
            {
                self[j] = self[j - d];
            }
            self[j] = temp;
        }
        d = d / 2;
    }
}
@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
            }
    return 0;
}
